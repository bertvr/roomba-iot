#pragma once

#include "config.hpp"
#include "roomba/roomba.hpp"
#include <CommandProcessor.h>
#include <LedMatrix.h>
#include <Pixel.h>

/**
 * @brief A class to controll the roomba via mqtt and controll the sensehat.
 * Uses composition
 */
class rpi {
  private:
    LedMatrix _leds;
    CommandProcessor _mqtt;
    Roomba _roomba;

    Sensors::BatteryState _batterystate_last;
    void drawLEDMatrix(Sensors::BatteryState state);

    /**
     * @brief Set the Speed of the roomba
     *
     * @param commmandParameters a string int between 0 and 500
     */
    void setSpeed(const std::vector<std::string> &commmandParameters);

    /**
     * @brief Set the State  / cleaning mode of the roomba
     *
     * @param commmandParameters a sting version of the enum class Roomba::State
     */
    void setState(const std::vector<std::string> &commmandParameters);

  public:
    /**
     * @brief Construct a new rpi object
     *
     * @param comPort comport to use for the serial link interface to the roomba
     */
    rpi(std::string comPort);

    /**
     * @brief Destroy the rpi object
     *
     */
    ~rpi();

    /**
     * @brief runs the application
     * @note this function never returns
     *
     */
    void run();
};

rpi::rpi(std::string comPort) : _mqtt(config::APP_NAME, config::CLIENT_NAME, config::MQTT_HOST, config::MQTT_HOST_PORT), _roomba(comPort) { _leds.clear(); }

rpi::~rpi() {}

void rpi::run() {
    std::this_thread::sleep_for(std::chrono::seconds(1));

    // Add commands
    _mqtt.registerCommand("setSpeed", std::bind(&rpi::setSpeed, this, std::placeholders::_1));
    _mqtt.registerCommand("setState", std::bind(&rpi::setState, this, std::placeholders::_1));

    while (1) {
        // Draw LEDS
        drawLEDMatrix(_roomba.getBattery());

        auto clients = {reinterpret_cast<mosqpp::mosquittopp *>(&_mqtt)};
        for (auto &&client : clients) {
            int rc = client->loop();
            if (rc) {
                // std::cerr << "-- MQTT reconnect" << std::endl;
                client->reconnect();
            }
        }
    }
}

void rpi::drawLEDMatrix(Sensors::BatteryState state) {
    // Check if different
    if (state != _batterystate_last) {
        _batterystate_last = state;

        // Draw
        Pixel color;
        if (Sensors::BatteryState::FULL == state) {
            color = Pixel::GREEN;
        } else if (Sensors::BatteryState::LOW == state) {
            color = Pixel::ORANGE;
        } else {
            color = Pixel::RED;
        }

        for (int x = _leds.MIN_X; x <= _leds.MAX_X; x++) {
            for (int y = _leds.MIN_Y; y <= _leds.MAX_Y; y++) {
                _leds.setPixel(x, y, color);
            }
        }
    }
}

void rpi::setSpeed(const std::vector<std::string> &commmandParameters) {
    // Print
    std::cout << "rpi::setSpeed callback. Data: " << std::endl;
    for (auto e : commmandParameters) {
        std::cout << e << std::endl;
    }
    std::cout << std::endl;

    // Get speed
    int16_t speed;

    speed = std::stoi(commmandParameters.at(0)); // guess

    // Set speed
    _roomba.setSpeed(speed);
}

void rpi::setState(const std::vector<std::string> &commmandParameters) {
    // Print
    std::cout << "rpi::setState callback. Data: " << std::endl;
    for (auto e : commmandParameters) {
        std::cout << e << std::endl;
    }
    std::cout << std::endl;

    // Get state
    Roomba::State state; //{ IDLE, BUMP_AND_TURN, JENS_MASTER_PLAN, SPOT_MODE }

    // guess
    if (commmandParameters.at(0) == "IDLE") {
        state = Roomba::State::IDLE;
    } else if (commmandParameters.at(0) == "BUMP_AND_TURN") {
        state = Roomba::State::BUMP_AND_TURN;
    } else if (commmandParameters.at(0) == "JENS_MASTER_PLAN") {
        state = Roomba::State::JENS_MASTER_PLAN;
    } else if (commmandParameters.at(0) == "SPOT_MODE") {
        state = Roomba::State::SPOT_INIT;
    } else {
        std::cerr << "State command not recognized: " << commmandParameters.at(0) << std::endl;
        return;
    }

    // Set state
    _roomba.setState(state);
}
