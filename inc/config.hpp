#pragma once
#include <string>

namespace config {
    enum class baudrate : unsigned int { SLOW = 19200, DEFAULT = 115200 };

    const std::string APP_NAME = "ESEiot/1718sem4";
    const std::string CLIENT_NAME = "RPI";
    const std::string MQTT_HOST = "test.mosquitto.org"; // broker
    const int MQTT_HOST_PORT = 1883;
    const std::string MQTT_TOPIC_ROOT{APP_NAME + "/" + CLIENT_NAME};
} // namespace config
