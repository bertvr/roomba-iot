
#pragma once

#include <cstdint>

namespace config {
    /**
     * @brief A enum containing command bytes
     *
     */
    enum class command : uint8_t {
        START = 128,
        BAUD_1 = 129,
        MODE_SAFE = 131,
        MODE_FULL = 132,
        DRIVE_DIRECT = 145,
        DRIVE = 137,
        VACUUM = 138,
        SENSORS = 142,
        LEDS = 139,
        SONG = 140,
        PLAY = 141
    };
} // namespace config
