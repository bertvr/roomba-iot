#pragma once

#include "roomba/component.hpp"
#include "serial_commands.hpp"

/**
 * @brief A class to access sensors of the roomba
 *
 */
class Sensors : Component {
  public:
    /**
     * @brief an enum that describes the state of charge of the roomba
     *
     */
    enum class BatteryState { FULL, LOW, EMPTY };

    /**
     * @brief an enum that describes the state of collision of the roomba
     *
     */
    enum class CollisionState { NONE, LEFT, RIGHT, BOTH };

    /**
     * @brief an enum that descibes which buttons are pressed on the roomba
     *
     */
    enum class ButtonState { NONE, PLAY, ADVANCE, BOTH };

    /**
     * @brief an enum that describes the state of a led, which can either be on or off.
     *
     */
    enum class LEDState { OFF, ON };

    /**
     * @brief an enum that describes the state of the power led, which can either be off, red, green or orange.
     *
     */
    enum class PowerLEDState { OFF, RED, GREEN, ORANGE };

    Sensors() = delete;

    /**
     * @brief Construct a new Sensors object, and automaticly calls the start function.
     *
     * @param serial seriallink to send commands to for the component
     */
    Sensors(SerialLink &serial) : Component(serial) { start(); }

    /**
     * @brief Destroy the Sensors object, and automatticly calls the stop function.
     *
     */
    ~Sensors() { stop(); };

    /**
     * @brief this starts / enables the component
     *
     */
    void start() { _enabled = true; }

    /**
     * @brief Stops / disables component
     *
     */
    void stop() { _enabled = false; }

    /**
     * @brief Get the Battery state of the roomba.
     *
     * @return BatteryState can either be FULL, LOW or EMPTY.
     */
    BatteryState getBattery() {
        if (_enabled) {
            // Get batt data
            uint16_t charge, capacity;
            std::vector<uint8_t> data;

            // Read battery charge
            data = _serial.writeRead({static_cast<uint8_t>(config::command::SENSORS), SENSOR_PACKET_BATTCHARGE_ID}, SENSOR_PACKET_BATTCHARGE_ID_LENGTH);

            charge = data.at(0) << 8;
            charge |= data.at(1);

            // Read battery capacity
            data.clear();
            data = _serial.writeRead({static_cast<uint8_t>(config::command::SENSORS), SENSOR_PACKET_BATTCAPACITY_ID}, SENSOR_PACKET_BATTCAPACITY_ID_LENGTH);

            capacity = data.at(0) << 8;
            capacity |= data.at(1);

            // Determine state
            float soc = (static_cast<float>(charge) / static_cast<float>(capacity));
            if (soc > 0.5f) {
                return BatteryState::FULL;
            } else if (soc > 0.25f) {
                return BatteryState::LOW;
            } else {
                return BatteryState::EMPTY;
            }
        }

        throw std::logic_error("Sensors::getBattery() called before start()");
    }

    /**
     * @brief Checks and returns the current Collision state of the roomba.
     *
     * @return CollisionState can be NONE, LEFT, RIGHT or BOTH.
     */
    CollisionState getCollision() {
        if (_enabled) {
            uint8_t state =
                (_serial.writeRead({static_cast<uint8_t>(config::command::SENSORS), SENSOR_PACKET_BUMP_ID}, SENSOR_PACKET_BUMP_ID_LENGTH).at(0)) & 0x03;

            if (state == 0x00) {
                return CollisionState::NONE;
            } else if (state == 0x01) {
                return CollisionState::RIGHT;
            } else if (state == 0x02) {
                return CollisionState::LEFT;
            } else {
                return CollisionState::BOTH;
            }
        }

        throw std::logic_error("Sensors::getCollision() called before start()");
    }

    /**
     * @brief Checks and returns the current state of buttons of the roomba.
     *
     * @return ButtonState, can be NONE, PLAY, ADVANCE or BOTH.
     */
    ButtonState isButtonPressed() {
        if (_enabled) {
            uint8_t state =
                (_serial.writeRead({static_cast<uint8_t>(config::command::SENSORS), SENSOR_PACKET_BUTTON_ID}, SENSOR_PACKET_BUTTON_ID_LENGTH)).at(1);

            if (state & 0x01 && state & 0x04) {
                return ButtonState::BOTH;
            } else if (state & 0x01) {
                return ButtonState::PLAY;
            } else if (state & 0x04) {
                return ButtonState::ADVANCE;
            } else {
                return ButtonState::NONE;
            }
        }

        throw std::logic_error("Sensors::isButtonPressed() called before start()");
    }

    /**
     * @brief Sets the color of the LEDs
     *
     * @param advance The state to set the advance button's led to.
     * @param play The state to set the play button's led to.
     * @param power The state to set the power level led to.
     */
    void setLED(LEDState advance, LEDState play, PowerLEDState power) {
        if (_enabled) {
            if (_prev_advance == advance && _prev_play == play && _prev_power == power) {
                return;
            }
            _prev_advance = advance;
            _prev_play = play;
            _prev_power = power;

            std::vector<uint8_t> data(4);

            data.at(0) = static_cast<uint8_t>(config::command::LEDS);

            data.at(1) = 0;
            if (LEDState::ON == advance) {
                data.at(1) |= 0x08;
            }
            if (LEDState::ON == play) {
                data.at(1) |= 0x02;
            }

            if (PowerLEDState::OFF == power) {
                data.at(2) = 0;
                data.at(3) = 0; // no brightness
            } else if (PowerLEDState::RED == power) {
                data.at(2) = 255;
                data.at(3) = 255; // full brightness
            } else if (PowerLEDState::GREEN == power) {
                data.at(2) = 0;
                data.at(3) = 255; // full brightness
            } else {              // orange
                data.at(2) = 127;
                data.at(3) = 255; // full brightness
            }

            _serial.write(data);

            return;
        }

        throw std::logic_error("Sensors::setLED() called before start()");
    }

  private:
    // Variables
    bool _enabled = false;

    LEDState _prev_advance = LEDState::OFF, _prev_play = LEDState::OFF;
    PowerLEDState _prev_power = PowerLEDState::OFF;

    // Constants
    static const uint8_t SENSOR_PACKET_BUMP_ID = 7;
    static const size_t SENSOR_PACKET_BUMP_ID_LENGTH = 1;

    static const uint8_t SENSOR_PACKET_BATTCHARGE_ID = 25;
    static const size_t SENSOR_PACKET_BATTCHARGE_ID_LENGTH = 2;

    static const uint8_t SENSOR_PACKET_BATTCAPACITY_ID = 26;
    static const size_t SENSOR_PACKET_BATTCAPACITY_ID_LENGTH = 2;

    static const uint8_t SENSOR_PACKET_BUTTON_ID = 18;
    static const size_t SENSOR_PACKET_BUTTON_ID_LENGTH = 2;
};
