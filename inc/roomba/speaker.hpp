#pragma once

#include "roomba/component.hpp"
#include "serial_commands.hpp"

/**
 * @brief A class to control the speaker of a roomba
 *
 */
class Speaker : Component {
  public:
    Speaker() = delete;

    /**
     * @brief Construct a new Speaker object
     *
     * @param serial seriallink to send commands to for the component
     */
    Speaker(SerialLink &serial) : Component(serial) { start(); }

    /**
     * @brief Stops and Destroys the Speaker object
     *
     */
    ~Speaker() { stop(); }

    /**
     * @brief this starts / enables the component.
     *
     */
    void start() { _enabled = true; }

    /**
     * @brief his stops / disabels the component.
     *
     */
    void stop() {
        if (_enabled) {
            _enabled = false;
        }
    }

    /**
     * @brief This initializes the speaker class by sending a song to the roomba.
     *
     */
    void begin() {
        // Send standard songs
        std::vector<uint8_t> data;
        data.push_back(static_cast<uint8_t>(config::command::SONG));
        data.push_back(0); // 0th song
        data.push_back(1);
        data.push_back(69);
        data.push_back(64);

        // Write data over serial
        _serial.write(data);

        // random
        data.clear();

        data.push_back(static_cast<uint8_t>(config::command::SONG));

        data.push_back(1);  // Song number
        data.push_back(11); // Song length (0x0A = 11)

        data.push_back(50); // Note
        data.push_back(16); // Note length

        data.push_back(49); // Note
        data.push_back(16); // Note length

        data.push_back(48); // Note
        data.push_back(16); // Note length

        data.push_back(47); // Note
        data.push_back(32); // Note length

        data.push_back(50); // Note
        data.push_back(8);  // Note length

        data.push_back(47); // Note
        data.push_back(8);  // Note length

        data.push_back(43); // Note
        data.push_back(16); // Note length

        data.push_back(42); // Note
        data.push_back(16); // Note length

        data.push_back(50); // Note
        data.push_back(16); // Note length

        data.push_back(38); // Note
        data.push_back(16); // Note length

        data.push_back(37); // Note
        data.push_back(32); // Note length

        _serial.write(data);

        // zelda
        data.clear();

        data.push_back(static_cast<uint8_t>(config::command::SONG));

        int duration = 18;

        data.push_back(2); // Song number
        data.push_back(4); // Song length

        data.push_back(69);       // Note
        data.push_back(duration); // Note length

        data.push_back(70);       // Note
        data.push_back(duration); // Note length

        data.push_back(71);       // Note
        data.push_back(duration); // Note length

        data.push_back(72);           // Note
        data.push_back(duration * 2); // Note length

        _serial.write(data);
    }

    /**
     * @brief This plays this a preloaded song
     *
     * @param song_index the index of a preloaded song
     */
    void play(int song_index) {
        if (_enabled) {
            // Write data over serial
            _serial.write({static_cast<uint8_t>(config::command::PLAY), static_cast<uint8_t>(song_index)});

            return;
        }

        throw std::logic_error("Speaker::play() called before start()");
    }

  private:
    // Variables
    bool _enabled = false;
    bool _prev_state = false;

    // Constants
    static const uint8_t VACUUM_ON = 7;
    static const uint8_t VACUUM_OFF = 0;
};
