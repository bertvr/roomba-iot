#pragma once

#include "roomba/component.hpp"
#include "serial_commands.hpp"

/**
 * @brief A class to control the vacuum of a roomba
 *
 */
class Vacuum : Component {
  public:
    Vacuum() = delete;
    /**
     * @brief Construct a new Vacuum object
     *
     * @param serial seriallink to send commands to for the component
     */
    Vacuum(SerialLink &serial) : Component(serial) {
        start();
        setState(false);
    }

    /**
     * @brief Destroy and stop the Vacuum object
     *
     */
    ~Vacuum() { stop(); }

    /**
     * @brief enables / inits the vacuum control
     *
     */
    void start() { _enabled = true; }

    /**
     * @brief stops and disables vacuum control
     *
     */
    void stop() {
        if (_enabled) {
            setState(false);
            _enabled = false;
        }
    }

    /**
     * @brief Set the State of the vacuum
     *
     * @param state True = vacuum on, False = vacuum off.
     */
    void setState(bool state) {
        if (_enabled) {
            if (_prev_state == state) {
                return;
            }
            _prev_state = state;

            // Write data over serial
            _serial.write({static_cast<uint8_t>(config::command::VACUUM), ((state == true) ? VACUUM_ON : VACUUM_OFF)});

            return;
        }

        throw std::logic_error("Vacuum::setState() called before start()");
    }

  private:
    // Variables
    bool _enabled = false;
    bool _prev_state = false;

    // Constants
    static const uint8_t VACUUM_ON = 7;
    static const uint8_t VACUUM_OFF = 0;
};
