#pragma once

#include "roomba/component.hpp"
#include "serial_commands.hpp"

/**
 * @brief A class to control the motors of the roomba.
 * @note inherits from component
 *
 */
class Motors : Component {
  public:
    /**
     * @brief Construct a new Motors object, and automaticly calls the start function.
     *
     * @param serial seriallink to send commands to for the component
     */
    Motors(SerialLink &serial) : Component(serial) { start(); }

    /**
     * @brief Destroy the Motors object, and automatticly calls the stop function.
     *
     */
    ~Motors() { stop(); };

    /**
     * @brief Enum for special cases in setMotor
     *
     */
    enum Direction : uint16_t { STRAIGHT = 0x7FFF, TANK_LEFT = 0x0001, TANK_RIGHT = 0xFFFF };

    /**
     * @brief starts / enables the motor
     *
     */
    void start() { _enabled = true; }

    /**
     * @brief stops and disables the motor
     *
     */
    void stop() {
        if (_enabled) {
            setMotorsDirect(0, 0);
            _enabled = false;
        }
    }

    /**
     * @brief Directly set the speed for the left and right motor
     *
     * @param left speed to set left motor in mm/s, clamped to 0 - 500.
     * @param right speed to set right motor in mm/s, clamped to 0 - 500.
     */
    void setMotorsDirect(int16_t left, int16_t right) {
        if (_enabled) {
            if (_prev_left = left && _prev_right == right) {
                return;
            }
            _prev_left = left;
            _prev_right = right;

            // Write data over serial
            _serial.write({static_cast<uint8_t>(config::command::DRIVE_DIRECT), static_cast<uint8_t>((right >> 8) & 0x000000FF),
                           static_cast<uint8_t>(right & 0x000000FF), static_cast<uint8_t>((left >> 8) & 0x000000FF), static_cast<uint8_t>(left & 0x000000FF)});

            return;
        }

        throw std::logic_error("Motor::setMotors() DRIVE_DIRECT called before start()");
    }

    /**
     * @brief Set the speed of the motors based on a velocity and radius.
     *
     * @param velocity speed of the roomba in mm/s
     * @param radius
     */
    void setMotors(int16_t velocity, int16_t radius) {
        if (_enabled) {
            if (_prev_velocity == velocity && _prev_radius == radius) {
                return;
            }
            _prev_velocity = velocity;
            _prev_radius = radius;

            // Write data over serial
            _serial.write({static_cast<uint8_t>(config::command::DRIVE), static_cast<uint8_t>((velocity >> 8) & 0x000000FF),
                           static_cast<uint8_t>(velocity & 0x000000FF), static_cast<uint8_t>((radius >> 8) & 0x000000FF),
                           static_cast<uint8_t>(radius & 0x000000FF)});

            return;
        }

        throw std::logic_error("Motor::setMotors() DRIVE called before start()");
    }

  private:
    // Variables
    bool _enabled = false;

    int16_t _prev_left = 0, _prev_right = 0;
    int16_t _prev_velocity = 0, _prev_radius = 0;
};
