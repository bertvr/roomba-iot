#pragma once

#include "config.hpp"
#include "roomba/motors.hpp"
#include "roomba/sensors.hpp"
#include "roomba/speaker.hpp"
#include "roomba/vacuum.hpp"
#include <SerialLink/SerialLink.h>
#include <atomic>
#include <chrono>
#include <cmath>
#include <limits>
#include <random>
#include <string>
#include <thread>

using namespace std::chrono_literals;
/**
 * @brief A class that represents a roomba, uses composition of components like motors and vacuum
 *
 */
class Roomba {
  public:
    enum class State;

  private:
    std::chrono::seconds _batteryInterval = 30s;
    int16_t _max_radius = 500;
    int16_t _min_radius = 5;
    SerialLink _serial;
    Motors _motors;
    Sensors _sensors;
    Vacuum _vacuum;
    Speaker _speaker;

    std::atomic<bool> _run;
    std::atomic<Roomba::State> _prev_state;
    std::atomic<Roomba::State> _state;
    std::atomic<Sensors::BatteryState> _batteryState;
    std::atomic<int16_t> _speed;
    std::thread _roombaThread;

    void _roombaLoop();

  public:
    enum class State { IDLE, BUMP_AND_TURN, JENS_MASTER_PLAN, SPOT_INIT, SPOT_MODE };

    Roomba(std::string comPort);
    ~Roomba();

    void setSpeed(int16_t speed) {
        _speed = std::min(500, abs(speed));
        std::cout << "Roomba speed set to " << _speed << std::endl;
    }
    void setState(Roomba::State state) {
        _state = state;
        std::cout << "Roomba state changed" << std::endl;
    }
    Roomba::State getState() { return _state; }
    Sensors::BatteryState getBattery() { return _batteryState; }
};

Roomba::Roomba(std::string comPort)
    : _serial(comPort, static_cast<unsigned int>(config::baudrate::DEFAULT)), _motors(_serial), _sensors(_serial), _vacuum(_serial), _speaker(_serial),
      _run(true), _prev_state(Roomba::State::IDLE), _state(Roomba::State::IDLE), _batteryState(Sensors::BatteryState::EMPTY), _speed(500),
      _roombaThread(&Roomba::_roombaLoop, this) {}

Roomba::~Roomba() {
    std::cout << "Bye Bye roombaloop shutting down!" << std::endl;
    _run = false;
    _roombaThread.join();
}

void Roomba::_roombaLoop() {

    std::this_thread::sleep_for(5s);
    std::cout << "Initing roomba!" << std::endl;

    // Send start command
    _serial.write({static_cast<uint8_t>(config::command::START), static_cast<uint8_t>(config::command::MODE_SAFE)});

    std::this_thread::sleep_for(1s);

    // set led to orange zo we know it is doing something
    _sensors.setLED(Sensors::LEDState::ON, Sensors::LEDState::ON, Sensors::PowerLEDState::ORANGE);

    std::this_thread::sleep_for(1s);

    _speaker.begin();
    _speaker.play(2);

    // create random number generator
    std::random_device r;
    std::uniform_int_distribution<int> uniform_dist(0, 1000);

    // Init roomba to idle
    _motors.start();
    _motors.setMotorsDirect(0, 0);
    _sensors.start();
    _vacuum.start();
    _vacuum.setState(false);

    std::chrono::time_point<std::chrono::system_clock> _prev_battery = std::chrono::system_clock::now();
    std::chrono::time_point<std::chrono::system_clock> _spot_radius_finished;
    int16_t radius = 0;
    while (_run) {
        // Poll battery in interval of _batteryInterval
        if (std::chrono::system_clock::now() - _prev_battery > _batteryInterval) {
            _prev_battery = std::chrono::system_clock::now();
            _batteryState = _sensors.getBattery();
        }

        switch (_state) {
        case Roomba::State::IDLE:
            _sensors.setLED(Sensors::LEDState::ON, Sensors::LEDState::ON, Sensors::PowerLEDState::GREEN);
            _motors.setMotorsDirect(0, 0);
            _vacuum.setState(false);
            break;
        case Roomba::State::BUMP_AND_TURN:
            _sensors.setLED(Sensors::LEDState::ON, Sensors::LEDState::ON, Sensors::PowerLEDState::ORANGE);
            _vacuum.setState(true);
            switch (_sensors.getCollision()) {
            case Sensors::CollisionState::NONE:
                _motors.setMotorsDirect(_speed, _speed);
                break;
            case Sensors::CollisionState::LEFT:
                _motors.setMotorsDirect(_speed, _speed / -2);
                std::this_thread::sleep_for(1s);
                break;
            case Sensors::CollisionState::RIGHT:
                _motors.setMotorsDirect(_speed / -2, _speed);
                std::this_thread::sleep_for(1s);
                break;
            case Sensors::CollisionState::BOTH:
                _motors.setMotorsDirect(-_speed, -_speed);
                std::this_thread::sleep_for(0.5s);
                if (uniform_dist(r) % 2 == 0) {
                    _motors.setMotorsDirect(_speed / -2, _speed);
                } else {
                    _motors.setMotorsDirect(_speed, _speed / -2);
                }
                std::this_thread::sleep_for(0.5s);
                break;
            default:
                _motors.setMotorsDirect(0, 0);
                break;
            }
            break;
        case Roomba::State::JENS_MASTER_PLAN:
            _sensors.setLED(Sensors::LEDState::ON, Sensors::LEDState::ON, Sensors::PowerLEDState::ORANGE);
            _vacuum.setState(true);
            if (uniform_dist(r) % 2 == 0) {
                _motors.setMotorsDirect(_speed, -_speed);
            } else {
                _motors.setMotorsDirect(-_speed, _speed);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(uniform_dist(r)));
            break;
        case Roomba::State::SPOT_INIT:
            std::cerr << "Spot init!" << std::endl;
            radius = _max_radius;
            _spot_radius_finished = std::chrono::system_clock::now() + std::chrono::seconds((int)((M_PI * 2 * radius) / _speed));
            _motors.setMotors(_speed, radius);
            setState(Roomba::State::SPOT_MODE);
            break;
        case Roomba::State::SPOT_MODE:
            _sensors.setLED(Sensors::LEDState::ON, Sensors::LEDState::ON, Sensors::PowerLEDState::ORANGE);
            _vacuum.setState(true);
            if (_sensors.getCollision() != Sensors::CollisionState::NONE) {
                std::cerr << "Exiting spot mode because collision!" << std::endl;
                setState(Roomba::State::IDLE);
            } else if (std::chrono::system_clock::now() > _spot_radius_finished) {
                radius = radius / 1.2;
                if (radius > 0) {
                    _spot_radius_finished = std::chrono::system_clock::now() + std::chrono::seconds((int)((M_PI * 2 * radius) / _speed));
                    _motors.setMotors(_speed, radius);
                    std::cerr << "Decreasing radius" << std::endl;
                } else {
                    setState(State::IDLE);
                    std::cerr << "Spot done!" << std::endl;
                }
            }
            break;
        default:
            _sensors.setLED(Sensors::LEDState::ON, Sensors::LEDState::ON, Sensors::PowerLEDState::ORANGE);
            _vacuum.setState(false);
            _motors.setMotorsDirect(0, 0);
            break;
        }
        _prev_state = _state.load();
    }
    _vacuum.setState(false);
    _motors.setMotorsDirect(0, 0);
}
