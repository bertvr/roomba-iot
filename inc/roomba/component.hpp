#pragma once

#include "SerialLink/SerialLink.h"

#include <stdexcept>
#include <vector>

/**
 * @brief An abstract base component containing basic functionality
 *
 */
class Component {
  public:
    /**
     * @brief Construct a new Component object
     *
     * @param serial seriallink to send commands to for the component
     */
    Component(SerialLink &serial) : _serial(serial) {}

    /**
     * @brief This function starts / enables the component
     * @note This function should be called before calling other member fuctions.
     *
     */
    virtual void start() = 0;

    /**
     * @brief This function stops / disables the component
     * @note The start function should be called before calling this member fuctions.
     *
     */
    virtual void stop() = 0;

  protected:
    // Serial
    SerialLink &_serial;
};
