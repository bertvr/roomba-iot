#include "rpi.hpp"
#include <exception>
#include <iostream>

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Invalid Use!" << std::endl;
        std::cout << "Usage:" << std::endl;
        std::cout << argv[0] << " <COM Port>" << std::endl;
        return -1;
    }
    std::cout << "Roomba-iot starting..." << std::endl;
    try {
        mosqpp::lib_init();
        rpi pi(argv[1]);
        pi.run();
    } catch (std::exception &e) {
        std::cout << "Error running roomba-iot: " << e.what() << std::endl;
    } catch (...) {
        std::cout << "Error running roomba-iot..." << std::endl;
    }
    std::cout << "Roomba-iot done, bye bye..." << std::endl;
}
