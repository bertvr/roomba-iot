#ifndef MQTTCONFIG_H
#define MQTTCONFIG_H

#include <string>

const char *getHostname();
const std::string HOSTNAME{getHostname()};

const std::string GROUP{"1718sem4"};

const int MQTT_KEEP_ALIVE{60};
const int MQTT_QoS_0{0};
const int MQTT_QoS_1{1};
const int MQTT_QoS_2{2};
const bool MQTT_RETAIN_OFF{false};
const bool MQTT_RETAIN_ON{true};

const std::string MQTT_LOCAL_BROKER{"127.0.0.1"};
const int MQTT_LOCAL_BROKER_PORT{1883};

const std::string MQTT_TOPIC_ROOT{"ESEiot/" + GROUP + "/" + HOSTNAME};

// TTN

// const std::string TTN_APP_ID{"han_demo_test_application"};
// const std::string TTN_APP_KEY{
//    "ttn-account-v2.wvrKQt7M6tCqjsqZt27Nx9XdaJ5IT-It9c5cqs42ZUw"};

const std::string TTN_APP_ID{"iot_on_fire_sffa"};
const std::string TTN_ACC_KEY{"ttn-account-v2.CUvICSZg71EP3VydoQ22__r9EfzteekLlRbR6NNLdIU"};
/// ttn-handler-eu
const std::string TTN_REGION{"eu"};

const std::string TTN_MQTT_HOST{TTN_REGION + ".thethings.network"};
const int TTN_MQTT_PORT{1883};
const int TTN_KEEP_ALIVE{120};

const std::string TTN_TOPIC_UP{TTN_APP_ID + "/devices/+/up"};
// const std::string GEN_TTN_TOPIC_DOWN(std::string dev_id){
//   return TTN_APP_ID + "/devices/" + dev_id +"/up";
//}

#endif
