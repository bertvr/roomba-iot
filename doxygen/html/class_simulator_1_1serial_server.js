var class_simulator_1_1serial_server =
[
    [ "connect", "class_simulator_1_1serial_server.html#a6a06fcc62ccedd57de24a53b0919d61f", null ],
    [ "disconnect", "class_simulator_1_1serial_server.html#a7fdd542a82d5fff71e5f7ebd44dca1fc", null ],
    [ "init", "class_simulator_1_1serial_server.html#a0bb86a935ea39c78900c833432a0e1fd", null ],
    [ "isConnected", "class_simulator_1_1serial_server.html#ab419d5a1a9f81a1e6670422313da3ce0", null ],
    [ "logDelegate", "class_simulator_1_1serial_server.html#a268c482db61f70859c7892db6981bfbd", null ],
    [ "send", "class_simulator_1_1serial_server.html#a37d53923044f353c85b676ff6b8aed4e", null ],
    [ "serialMsgReceivedDelegate", "class_simulator_1_1serial_server.html#a103044e666b0ee47d072e6e10987c70b", null ],
    [ "setLogFunction", "class_simulator_1_1serial_server.html#a513cb4250e0fcf74bd6ab898f5a0c6b5", null ],
    [ "setMessageHandler", "class_simulator_1_1serial_server.html#ac08770657a8b1dfc597de3e3c04fcb6c", null ]
];