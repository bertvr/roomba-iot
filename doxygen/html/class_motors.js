var class_motors =
[
    [ "Direction", "class_motors.html#afad186983c7a874d6dd2117d4501b598", [
      [ "STRAIGHT", "class_motors.html#afad186983c7a874d6dd2117d4501b598a921394920676c24f603cc9181785e356", null ],
      [ "TANK_LEFT", "class_motors.html#afad186983c7a874d6dd2117d4501b598acf5e91f372e0707dc679b656db9184b4", null ],
      [ "TANK_RIGHT", "class_motors.html#afad186983c7a874d6dd2117d4501b598aa176c8add04a92c06f3b6a14ccac602b", null ]
    ] ],
    [ "Motors", "class_motors.html#a8458fd882128a15951ec174da3abda4a", null ],
    [ "~Motors", "class_motors.html#ab9d527a65397c54bd24c364b22b31317", null ],
    [ "setMotors", "class_motors.html#addc816cc580ad8f52b8a47375c199a10", null ],
    [ "setMotorsDirect", "class_motors.html#a27581c14d743fa6488284c03c64fe154", null ],
    [ "start", "class_motors.html#ac00cdbceba93410b72638cc5b1071deb", null ],
    [ "stop", "class_motors.html#a453e43aaa11a1f41ebe0164b9eabc1d0", null ],
    [ "_enabled", "class_motors.html#a61047fa5369d3af36612181fb19a1a34", null ],
    [ "_prev_left", "class_motors.html#adca0ea83308b8fc193cd1edfd323569d", null ],
    [ "_prev_radius", "class_motors.html#a215934306ca94d00a6afe28e24734f97", null ],
    [ "_prev_right", "class_motors.html#a557e2adf5dc71a2e7e358da6b6f37fcb", null ],
    [ "_prev_velocity", "class_motors.html#a3cf24274b87ea9717b364eed34bf8dbc", null ]
];