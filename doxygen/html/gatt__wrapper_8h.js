var gatt__wrapper_8h =
[
    [ "DBG", "gatt__wrapper_8h.html#ae7b1059b840a9e601cfbf2c11cccfa80", null ],
    [ "gatt_connect", "gatt__wrapper_8h.html#a1e6daecb3d872d8075532234eb604bd2", null ],
    [ "gatt_disconnect", "gatt__wrapper_8h.html#a73f59caaedd24b7668f370ba3c645823", null ],
    [ "gatt_exit", "gatt__wrapper_8h.html#a1ddb2f332863a53b3ec2cc7025e251c6", null ],
    [ "gatt_read", "gatt__wrapper_8h.html#abb7116e5b740fbf33c7f0bbb2f0008e7", null ],
    [ "gatt_start", "gatt__wrapper_8h.html#a2fc91f61d5e168adf0657aefaa590639", null ],
    [ "gatt_write", "gatt__wrapper_8h.html#a0f9160570d2ce838fede81889f81e3a1", null ]
];