var class_sensors =
[
    [ "BatteryState", "class_sensors.html#aa7de72a336f0c2d6c0426aa2ff9bca46", [
      [ "FULL", "class_sensors.html#aa7de72a336f0c2d6c0426aa2ff9bca46aba7de5bc6888294e5884b024a4c894f1", null ],
      [ "LOW", "class_sensors.html#aa7de72a336f0c2d6c0426aa2ff9bca46a41bc94cbd8eebea13ce0491b2ac11b88", null ],
      [ "EMPTY", "class_sensors.html#aa7de72a336f0c2d6c0426aa2ff9bca46aba2b45bdc11e2a4a6e86aab2ac693cbb", null ]
    ] ],
    [ "ButtonState", "class_sensors.html#af320a21bbf5f39af07241c51941b268c", [
      [ "NONE", "class_sensors.html#af320a21bbf5f39af07241c51941b268cab50339a10e1de285ac99d4c3990b8693", null ],
      [ "PLAY", "class_sensors.html#af320a21bbf5f39af07241c51941b268ca6a216efc529825c60a4a4c0bc99ad77f", null ],
      [ "ADVANCE", "class_sensors.html#af320a21bbf5f39af07241c51941b268cacc52554c5f7b607e139b80f23b8cf5d0", null ],
      [ "BOTH", "class_sensors.html#af320a21bbf5f39af07241c51941b268ca6328e5e3186c227a021ef2ff77e40197", null ]
    ] ],
    [ "CollisionState", "class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5", [
      [ "NONE", "class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "LEFT", "class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5a684d325a7303f52e64011467ff5c5758", null ],
      [ "RIGHT", "class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5a21507b40c80068eda19865706fdc2403", null ],
      [ "BOTH", "class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5a6328e5e3186c227a021ef2ff77e40197", null ]
    ] ],
    [ "LEDState", "class_sensors.html#a82d40d8e798ce1c05783750b683a9c8c", [
      [ "OFF", "class_sensors.html#a82d40d8e798ce1c05783750b683a9c8ca88559a0cfd8250c9d65970cc145c92d4", null ],
      [ "ON", "class_sensors.html#a82d40d8e798ce1c05783750b683a9c8ca90651ebea9a35ec4e018c8157492e17c", null ]
    ] ],
    [ "PowerLEDState", "class_sensors.html#ae5b06e49e12906a7826e8b29c70ad99b", [
      [ "OFF", "class_sensors.html#ae5b06e49e12906a7826e8b29c70ad99ba88559a0cfd8250c9d65970cc145c92d4", null ],
      [ "RED", "class_sensors.html#ae5b06e49e12906a7826e8b29c70ad99baa2d9547b5d3dd9f05984475f7c926da0", null ],
      [ "GREEN", "class_sensors.html#ae5b06e49e12906a7826e8b29c70ad99ba9de0e5dd94e861317e74964bed179fa0", null ],
      [ "ORANGE", "class_sensors.html#ae5b06e49e12906a7826e8b29c70ad99ba5b6490317b6f7270bc3ab5ffd07c1f52", null ]
    ] ],
    [ "Sensors", "class_sensors.html#a80ef7a0bdabe2ef16fe925b893adc934", null ],
    [ "Sensors", "class_sensors.html#a01297c9da9b1f53078a86681f84b4f93", null ],
    [ "~Sensors", "class_sensors.html#ae8757a85e47bb4d61ef3e33cdf3e0d87", null ],
    [ "getBattery", "class_sensors.html#a1203db3d32437ddc6e16e91f7d4e3647", null ],
    [ "getCollision", "class_sensors.html#a979848c8ae06a35a4b6ccd50bf9583e3", null ],
    [ "isButtonPressed", "class_sensors.html#adc6ee83d6d5f934d2398f3560408b85d", null ],
    [ "setLED", "class_sensors.html#a40aad014bd8537227ca9f9c76ebde4fb", null ],
    [ "start", "class_sensors.html#a3a872bcf58946d96148d9b5b788e992e", null ],
    [ "stop", "class_sensors.html#ac492b0b5f091b5ee16bfc670db727c23", null ],
    [ "_enabled", "class_sensors.html#ac10045a4c018ff0e2f86197147603495", null ],
    [ "_prev_advance", "class_sensors.html#a0b53703dd936e7b5a6d8343cc9796e97", null ],
    [ "_prev_play", "class_sensors.html#a4713856cbdd0ebf0e6638556e65d4419", null ],
    [ "_prev_power", "class_sensors.html#a650a0dabd1468e0a80bfb6de429e068d", null ],
    [ "SENSOR_PACKET_BATTCAPACITY_ID", "class_sensors.html#affc7fed10ee73807076f9448d4bc515a", null ],
    [ "SENSOR_PACKET_BATTCAPACITY_ID_LENGTH", "class_sensors.html#a3792df9030b5bee4468eaa784eb6e2fa", null ],
    [ "SENSOR_PACKET_BATTCHARGE_ID", "class_sensors.html#a7b443da9af7e2b564c2629c0ba5a2fb6", null ],
    [ "SENSOR_PACKET_BATTCHARGE_ID_LENGTH", "class_sensors.html#a34394a3d1ce6985fc4561296caa64a4a", null ],
    [ "SENSOR_PACKET_BUMP_ID", "class_sensors.html#a33a237d5f14cc13ceb6fe81e7397aaf4", null ],
    [ "SENSOR_PACKET_BUMP_ID_LENGTH", "class_sensors.html#a4cdcbf0661190e1330e1dc5816aca34e", null ],
    [ "SENSOR_PACKET_BUTTON_ID", "class_sensors.html#a4bbd9782daecb51f33b82653ef37a88e", null ],
    [ "SENSOR_PACKET_BUTTON_ID_LENGTH", "class_sensors.html#aa607fdc76049808a9cee40ae60a14198", null ]
];