var searchData=
[
  ['sensors_243',['SENSORS',['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180ea3f9d1c3652820e6c9716f628c6f0b5af',1,'config']]],
  ['slow_244',['SLOW',['../namespaceconfig.html#ac4ecee8943cfdb37ac115fb3b4b90a0aa0e3066cbbd284dce8b76e7c4620d6d75',1,'config']]],
  ['song_245',['SONG',['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180eac184d7ee5f39c2a4112a9c14640488fa',1,'config']]],
  ['spot_5finit_246',['SPOT_INIT',['../class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16ea25a1c3af3a3ef8574887100a95c82455',1,'Roomba']]],
  ['spot_5fmode_247',['SPOT_MODE',['../class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16eadc7470de7efcba093bb26e11b6d75929',1,'Roomba']]],
  ['start_248',['START',['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180eab078ffd28db767c502ac367053f6e0ac',1,'config']]],
  ['straight_249',['STRAIGHT',['../class_motors.html#afad186983c7a874d6dd2117d4501b598a921394920676c24f603cc9181785e356',1,'Motors']]]
];
