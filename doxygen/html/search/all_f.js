var searchData=
[
  ['red_76',['RED',['../class_sensors.html#ae5b06e49e12906a7826e8b29c70ad99baa2d9547b5d3dd9f05984475f7c926da0',1,'Sensors']]],
  ['right_77',['RIGHT',['../class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5a21507b40c80068eda19865706fdc2403',1,'Sensors']]],
  ['roomba_78',['Roomba',['../class_roomba.html',1,'Roomba'],['../class_roomba.html#aa55e8710eb3be134529fc667150be4c0',1,'Roomba::Roomba()']]],
  ['roomba_2ehpp_79',['roomba.hpp',['../roomba_8hpp.html',1,'']]],
  ['rpi_80',['rpi',['../classrpi.html',1,'rpi'],['../classrpi.html#a9fd1b749d05e90c83158e511ac943f9d',1,'rpi::rpi()']]],
  ['rpi_2ehpp_81',['rpi.hpp',['../rpi_8hpp.html',1,'']]],
  ['run_82',['run',['../classrpi.html#ad95f77a0bf45ef6f583b6262b62e1283',1,'rpi']]]
];
