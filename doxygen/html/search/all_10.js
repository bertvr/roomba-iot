var searchData=
[
  ['sensor_5fpacket_5fbattcapacity_5fid_83',['SENSOR_PACKET_BATTCAPACITY_ID',['../class_sensors.html#affc7fed10ee73807076f9448d4bc515a',1,'Sensors']]],
  ['sensor_5fpacket_5fbattcapacity_5fid_5flength_84',['SENSOR_PACKET_BATTCAPACITY_ID_LENGTH',['../class_sensors.html#a3792df9030b5bee4468eaa784eb6e2fa',1,'Sensors']]],
  ['sensor_5fpacket_5fbattcharge_5fid_85',['SENSOR_PACKET_BATTCHARGE_ID',['../class_sensors.html#a7b443da9af7e2b564c2629c0ba5a2fb6',1,'Sensors']]],
  ['sensor_5fpacket_5fbattcharge_5fid_5flength_86',['SENSOR_PACKET_BATTCHARGE_ID_LENGTH',['../class_sensors.html#a34394a3d1ce6985fc4561296caa64a4a',1,'Sensors']]],
  ['sensor_5fpacket_5fbump_5fid_87',['SENSOR_PACKET_BUMP_ID',['../class_sensors.html#a33a237d5f14cc13ceb6fe81e7397aaf4',1,'Sensors']]],
  ['sensor_5fpacket_5fbump_5fid_5flength_88',['SENSOR_PACKET_BUMP_ID_LENGTH',['../class_sensors.html#a4cdcbf0661190e1330e1dc5816aca34e',1,'Sensors']]],
  ['sensor_5fpacket_5fbutton_5fid_89',['SENSOR_PACKET_BUTTON_ID',['../class_sensors.html#a4bbd9782daecb51f33b82653ef37a88e',1,'Sensors']]],
  ['sensor_5fpacket_5fbutton_5fid_5flength_90',['SENSOR_PACKET_BUTTON_ID_LENGTH',['../class_sensors.html#aa607fdc76049808a9cee40ae60a14198',1,'Sensors']]],
  ['sensors_91',['Sensors',['../class_sensors.html',1,'Sensors'],['../class_sensors.html#a80ef7a0bdabe2ef16fe925b893adc934',1,'Sensors::Sensors()=delete'],['../class_sensors.html#a01297c9da9b1f53078a86681f84b4f93',1,'Sensors::Sensors(SerialLink &amp;serial)'],['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180ea3f9d1c3652820e6c9716f628c6f0b5af',1,'config::SENSORS()']]],
  ['sensors_2ehpp_92',['sensors.hpp',['../sensors_8hpp.html',1,'']]],
  ['serial_5fcommands_2ehpp_93',['serial_commands.hpp',['../serial__commands_8hpp.html',1,'']]],
  ['setled_94',['setLED',['../class_sensors.html#a40aad014bd8537227ca9f9c76ebde4fb',1,'Sensors']]],
  ['setmotors_95',['setMotors',['../class_motors.html#addc816cc580ad8f52b8a47375c199a10',1,'Motors']]],
  ['setmotorsdirect_96',['setMotorsDirect',['../class_motors.html#a27581c14d743fa6488284c03c64fe154',1,'Motors']]],
  ['setspeed_97',['setSpeed',['../class_roomba.html#aea0d634dff83ce74a5c092f84ccc3489',1,'Roomba::setSpeed()'],['../classrpi.html#a232413d0cf133c92a41bbda31c44d997',1,'rpi::setSpeed()']]],
  ['setstate_98',['setState',['../class_roomba.html#a257deb204634d353f5fbb94c09c8fb7b',1,'Roomba::setState()'],['../class_vacuum.html#a9d2971121b4a69cf622e1312d16e1a7b',1,'Vacuum::setState()'],['../classrpi.html#a8358f5f304d9056245b32e9502e74ef8',1,'rpi::setState()']]],
  ['slow_99',['SLOW',['../namespaceconfig.html#ac4ecee8943cfdb37ac115fb3b4b90a0aa0e3066cbbd284dce8b76e7c4620d6d75',1,'config']]],
  ['song_100',['SONG',['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180eac184d7ee5f39c2a4112a9c14640488fa',1,'config']]],
  ['speaker_101',['Speaker',['../class_speaker.html',1,'Speaker'],['../class_speaker.html#a16e25f11062f8afca2664befd67bba86',1,'Speaker::Speaker()=delete'],['../class_speaker.html#abf777671c329d7b643007ee7f8232d6f',1,'Speaker::Speaker(SerialLink &amp;serial)']]],
  ['speaker_2ehpp_102',['speaker.hpp',['../speaker_8hpp.html',1,'']]],
  ['spot_5finit_103',['SPOT_INIT',['../class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16ea25a1c3af3a3ef8574887100a95c82455',1,'Roomba']]],
  ['spot_5fmode_104',['SPOT_MODE',['../class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16eadc7470de7efcba093bb26e11b6d75929',1,'Roomba']]],
  ['start_105',['start',['../class_component.html#adef0f642e0ed12a9b29afe587b912972',1,'Component::start()'],['../class_motors.html#ac00cdbceba93410b72638cc5b1071deb',1,'Motors::start()'],['../class_sensors.html#a3a872bcf58946d96148d9b5b788e992e',1,'Sensors::start()'],['../class_speaker.html#ac636778f00fede863e836c31d4db068a',1,'Speaker::start()'],['../class_vacuum.html#a54ec5e56215e32d1c3c537ff4794915e',1,'Vacuum::start()'],['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180eab078ffd28db767c502ac367053f6e0ac',1,'config::START()']]],
  ['state_106',['State',['../class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16e',1,'Roomba']]],
  ['stop_107',['stop',['../class_component.html#a6871e1e4327dbe32dde28ce87fc1820e',1,'Component::stop()'],['../class_motors.html#a453e43aaa11a1f41ebe0164b9eabc1d0',1,'Motors::stop()'],['../class_sensors.html#ac492b0b5f091b5ee16bfc670db727c23',1,'Sensors::stop()'],['../class_speaker.html#af2602607ad8c5d7305ae35cad20fc775',1,'Speaker::stop()'],['../class_vacuum.html#ab2ec5d94d77af106451046e67aa78a62',1,'Vacuum::stop()']]],
  ['straight_108',['STRAIGHT',['../class_motors.html#afad186983c7a874d6dd2117d4501b598a921394920676c24f603cc9181785e356',1,'Motors']]]
];
