var searchData=
[
  ['batterystate_29',['BatteryState',['../class_sensors.html#aa7de72a336f0c2d6c0426aa2ff9bca46',1,'Sensors']]],
  ['baud_5f1_30',['BAUD_1',['../namespaceconfig.html#a83460fbf3430a0bdad7b2ead0a27180ea8d0272c44ddb6f3095c79f60e0d7a2bf',1,'config']]],
  ['baudrate_31',['baudrate',['../namespaceconfig.html#ac4ecee8943cfdb37ac115fb3b4b90a0a',1,'config']]],
  ['begin_32',['begin',['../class_speaker.html#a88e6da5ffda6b44800d1d2699836d16f',1,'Speaker']]],
  ['both_33',['BOTH',['../class_sensors.html#a0a4d4aacdcb89afed08a6daf292ed6f5a6328e5e3186c227a021ef2ff77e40197',1,'Sensors::BOTH()'],['../class_sensors.html#af320a21bbf5f39af07241c51941b268ca6328e5e3186c227a021ef2ff77e40197',1,'Sensors::BOTH()']]],
  ['bump_5fand_5fturn_34',['BUMP_AND_TURN',['../class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16eaab18afd6bc62138c71c959b07f66db47',1,'Roomba']]],
  ['buttonstate_35',['ButtonState',['../class_sensors.html#af320a21bbf5f39af07241c51941b268c',1,'Sensors']]]
];
