var indexSectionsWithContent =
{
  0: "_abcdefgijlmnoprstv~",
  1: "cmrsv",
  2: "c",
  3: "cmrsv",
  4: "_bcdgimprsv~",
  5: "_acmsv",
  6: "bcdlps",
  7: "abdefgijlmnoprstv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator"
};

