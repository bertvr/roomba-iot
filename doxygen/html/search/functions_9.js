var searchData=
[
  ['sensors_153',['Sensors',['../class_sensors.html#a80ef7a0bdabe2ef16fe925b893adc934',1,'Sensors::Sensors()=delete'],['../class_sensors.html#a01297c9da9b1f53078a86681f84b4f93',1,'Sensors::Sensors(SerialLink &amp;serial)']]],
  ['setled_154',['setLED',['../class_sensors.html#a40aad014bd8537227ca9f9c76ebde4fb',1,'Sensors']]],
  ['setmotors_155',['setMotors',['../class_motors.html#addc816cc580ad8f52b8a47375c199a10',1,'Motors']]],
  ['setmotorsdirect_156',['setMotorsDirect',['../class_motors.html#a27581c14d743fa6488284c03c64fe154',1,'Motors']]],
  ['setspeed_157',['setSpeed',['../class_roomba.html#aea0d634dff83ce74a5c092f84ccc3489',1,'Roomba::setSpeed()'],['../classrpi.html#a232413d0cf133c92a41bbda31c44d997',1,'rpi::setSpeed()']]],
  ['setstate_158',['setState',['../class_roomba.html#a257deb204634d353f5fbb94c09c8fb7b',1,'Roomba::setState()'],['../class_vacuum.html#a9d2971121b4a69cf622e1312d16e1a7b',1,'Vacuum::setState()'],['../classrpi.html#a8358f5f304d9056245b32e9502e74ef8',1,'rpi::setState()']]],
  ['speaker_159',['Speaker',['../class_speaker.html#a16e25f11062f8afca2664befd67bba86',1,'Speaker::Speaker()=delete'],['../class_speaker.html#abf777671c329d7b643007ee7f8232d6f',1,'Speaker::Speaker(SerialLink &amp;serial)']]],
  ['start_160',['start',['../class_component.html#adef0f642e0ed12a9b29afe587b912972',1,'Component::start()'],['../class_motors.html#ac00cdbceba93410b72638cc5b1071deb',1,'Motors::start()'],['../class_sensors.html#a3a872bcf58946d96148d9b5b788e992e',1,'Sensors::start()'],['../class_speaker.html#ac636778f00fede863e836c31d4db068a',1,'Speaker::start()'],['../class_vacuum.html#a54ec5e56215e32d1c3c537ff4794915e',1,'Vacuum::start()']]],
  ['stop_161',['stop',['../class_component.html#a6871e1e4327dbe32dde28ce87fc1820e',1,'Component::stop()'],['../class_motors.html#a453e43aaa11a1f41ebe0164b9eabc1d0',1,'Motors::stop()'],['../class_sensors.html#ac492b0b5f091b5ee16bfc670db727c23',1,'Sensors::stop()'],['../class_speaker.html#af2602607ad8c5d7305ae35cad20fc775',1,'Speaker::stop()'],['../class_vacuum.html#ab2ec5d94d77af106451046e67aa78a62',1,'Vacuum::stop()']]]
];
