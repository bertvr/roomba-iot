var class_vacuum =
[
    [ "Vacuum", "class_vacuum.html#a1e23a25e36e9ad59fcaf9a0a657ac410", null ],
    [ "Vacuum", "class_vacuum.html#ab2bdde7669774559ef53a8dc122ef698", null ],
    [ "~Vacuum", "class_vacuum.html#a4ee42dfa1bb599d67ee1584173f5a715", null ],
    [ "setState", "class_vacuum.html#a9d2971121b4a69cf622e1312d16e1a7b", null ],
    [ "start", "class_vacuum.html#a54ec5e56215e32d1c3c537ff4794915e", null ],
    [ "stop", "class_vacuum.html#ab2ec5d94d77af106451046e67aa78a62", null ],
    [ "_enabled", "class_vacuum.html#af20b2ec9ed872b53a1c30b1441b8fa48", null ],
    [ "_prev_state", "class_vacuum.html#a3e8ffb7d16a5e041d499bac3aa875db3", null ],
    [ "VACUUM_OFF", "class_vacuum.html#a65cc3a6e45bb76ac2e28b879842126f8", null ],
    [ "VACUUM_ON", "class_vacuum.html#a17f05aec2aeb93e1812db69394c9e0dd", null ]
];