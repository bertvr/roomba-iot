var gatt__wrapper_8c =
[
    [ "gatt_connect", "gatt__wrapper_8c.html#a1e6daecb3d872d8075532234eb604bd2", null ],
    [ "gatt_disconnect", "gatt__wrapper_8c.html#a73f59caaedd24b7668f370ba3c645823", null ],
    [ "gatt_exit", "gatt__wrapper_8c.html#a1ddb2f332863a53b3ec2cc7025e251c6", null ],
    [ "gatt_read", "gatt__wrapper_8c.html#abb7116e5b740fbf33c7f0bbb2f0008e7", null ],
    [ "gatt_start", "gatt__wrapper_8c.html#a2fc91f61d5e168adf0657aefaa590639", null ],
    [ "gatt_write", "gatt__wrapper_8c.html#a0f9160570d2ce838fede81889f81e3a1", null ],
    [ "fdX", "gatt__wrapper_8c.html#a0273ebfd2127b02d64f2fb197a265761", null ],
    [ "fdY", "gatt__wrapper_8c.html#ab73ddfc099afb2d78e27bcae01e27332", null ]
];