var class_roomba =
[
    [ "State", "class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16e", [
      [ "IDLE", "class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16eaa5daf7f2ebbba4975d61dab1c40188c7", null ],
      [ "BUMP_AND_TURN", "class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16eaab18afd6bc62138c71c959b07f66db47", null ],
      [ "JENS_MASTER_PLAN", "class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16ea845dcdab43d2811a265d36e68778d999", null ],
      [ "SPOT_INIT", "class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16ea25a1c3af3a3ef8574887100a95c82455", null ],
      [ "SPOT_MODE", "class_roomba.html#a3488f2c19a91bbb7a101b13f5604b16eadc7470de7efcba093bb26e11b6d75929", null ]
    ] ],
    [ "Roomba", "class_roomba.html#aa55e8710eb3be134529fc667150be4c0", null ],
    [ "~Roomba", "class_roomba.html#aab92a0ccb84bc12bb3be637ef761c63b", null ],
    [ "_roombaLoop", "class_roomba.html#a71676d08589bc2358ee94a6008f6a001", null ],
    [ "getBattery", "class_roomba.html#a7cc6006f0d4d7529201c3a546b597e91", null ],
    [ "getState", "class_roomba.html#ad905cf3562fc845c071be7076a25df0d", null ],
    [ "setSpeed", "class_roomba.html#aea0d634dff83ce74a5c092f84ccc3489", null ],
    [ "setState", "class_roomba.html#a257deb204634d353f5fbb94c09c8fb7b", null ],
    [ "_batteryInterval", "class_roomba.html#a4346eb180edfc194da1198ed0415aacb", null ],
    [ "_batteryState", "class_roomba.html#a026670d5823db4a8a263c8b8bedeae8e", null ],
    [ "_max_radius", "class_roomba.html#a5585aeb94620298417892c59b576af31", null ],
    [ "_min_radius", "class_roomba.html#a038a0d619a7714d39baef4459ebb7428", null ],
    [ "_motors", "class_roomba.html#a04e335b26e80a768745477517eaf6f6b", null ],
    [ "_prev_state", "class_roomba.html#a6a2af1babfd6229c2e637b9ca7873045", null ],
    [ "_roombaThread", "class_roomba.html#a97d760984ea3f4b625424870e0cb5d24", null ],
    [ "_run", "class_roomba.html#a0262dd37352d51ebea3e9f9906f6ad05", null ],
    [ "_sensors", "class_roomba.html#abae359ac6ba9d146054074fe99017ffa", null ],
    [ "_serial", "class_roomba.html#a18469d128658db6f653859b38e412fb5", null ],
    [ "_speaker", "class_roomba.html#a7101311efe90acf86016d24d2179bdc2", null ],
    [ "_speed", "class_roomba.html#a7366dc086dc4935868b53f7a8c14b210", null ],
    [ "_state", "class_roomba.html#a443ecc94a6bf8b16bd11c98ed071458c", null ],
    [ "_vacuum", "class_roomba.html#a05d522ee0fb107a0154ae6f6685fda55", null ]
];