var WebSocket_MQTT_Broker_URL = "";
var MQTT_Client_ID = "";
var MQTT_Topic = "";
var MQTT_Client = "";
var MQTT_topic_root = "";
var slider;

function setupInputEvent() {
    slider = document.getElementById("myRange");
    slider.addEventListener("input", mqtt_Publish_Speed);
}

function mqtt_Connect_with_Broker() {
    // Set variables
    WebSocket_MQTT_Broker_URL =
        document.getElementById("txt_MQTT_Broker_URL").value;
    MQTT_Client_ID =
        document.getElementById("txt_MQTT_Client_ID").value;
    MQTT_topic_root =
        document.getElementById("txt_MQTT_topicroot").value;

    // Create a MQTT Client nstance
    MQTT_Client =
        new Paho.MQTT.Client(WebSocket_MQTT_Broker_URL, MQTT_Client_ID);

    // set callback handlers
    MQTT_Client.onConnectionLost = onConnectionLost;
    //MQTT_Client.onMessageArrived = onMessageArrived;

    MQTT_Client.connect({
        onSuccess: onConnect
    });

}

// Called when the client connects
function onConnect() {
    console.log("Connected to " + WebSocket_MQTT_Broker_URL);

    document.getElementById("btn_Connect_to_Broker").disabled = true;
    document.getElementById("txt_MQTT_Broker_URL").disabled = true;
    document.getElementById("txt_MQTT_Client_ID").disabled = true;

    document.getElementById("idle").disabled = false;
    document.getElementById("jens").disabled = false;
    document.getElementById("spot").disabled = false;
    document.getElementById("bump").disabled = false;
    document.getElementById("myRange").disabled = false;
}

// Called when the client loses its connection
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("ERROR: connection lost with MQTT Broker: " +
            "\"" + responseObject.errorMessage + "\"");
        document.getElementById("btn_Connect_to_Broker").disabled = false;
        document.getElementById("txt_MQTT_Broker_URL").disabled = false;
        document.getElementById("txt_MQTT_Client_ID").disabled = false;

        document.getElementById("idle").disabled = true;
        document.getElementById("jens").disabled = true;
        document.getElementById("spot").disabled = true;
        document.getElementById("bump").disabled = true;
        document.getElementById("myRange").disabled = true;
    }
    //MQTT_Client.reconnect();
}

function mqtt_Publish_Speed() {
    message =
        new Paho.MQTT.Message("setSpeed " + slider.value);
    message.destinationName = document.getElementById("txt_MQTT_topicroot").value;

    console.log(message.destinationName);

    if (Boolean(message.destinationName)) {
        MQTT_Client.send(message);
    } else {
        alert("Error sending message!");
    }
}

function mqtt_Publish_STATE(name) {
    message = new Paho.MQTT.Message("setState " + name);
    message.destinationName = document.getElementById("txt_MQTT_topicroot").value;

    console.log(message.destinationName);
    console.log(message.payloadString);

    if (Boolean(message.destinationName)) {
        MQTT_Client.send(message);
    } else {
        alert("Error sending message!");
    }
}


// Document Ready Event
$(document).ready(function () {
    //Set default MQTT Broker WebSocket URL
    document.getElementById("txt_MQTT_Broker_URL").value =
        "wss://test.mosquitto.org:8081/ws";//"wss://iot.eclipse.org:443/ws";
    document.getElementById("txt_MQTT_topicroot").value =
        "ESEiot/1718sem4/RPI/command";
    //Generate Random MQTT Clinet ID
    gen_MQTT_Client_ID();
    document.getElementById("myRange").disabled = true;
    document.getElementById("idle").disabled = true;
    document.getElementById("jens").disabled = true;
    document.getElementById("spot").disabled = true;
    document.getElementById("bump").disabled = true;
    setupInputEvent();
    //mqtt_Connect_with_Broker();
})

// Randomly generate Client ID
function gen_MQTT_Client_ID() {
    var mqttid = document.getElementById("txt_MQTT_Client_ID");
    mqttid.oninput = function () {
        MQTT_Client_ID = mqttid.value;
    }
    mqttid.value =
        Math.floor(100000000000 + Math.random() * 900000000000);
    MQTT_Client_ID = mqttid.value;
}